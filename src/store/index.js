import { createStore, combineReducers } from 'redux';

import productReducer from './modules/products/reducer';
import cartReducer from './modules/cart/reducer';

const reducers = combineReducers({
    products: productReducer,
    cart: cartReducer,
})

const store = createStore(reducers);

export default store;