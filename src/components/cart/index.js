import { useSelector } from 'react-redux';
import Product from '../product'

const Cart = () => {
    const cart = useSelector((state) => state.cart);
    console.log(cart)
    return (
        <div>
            <h1>Meu Carrindo de Compras</h1>
            {cart.map((product, index) => 
                <Product key={ index } product={ product } isRemovable/>,
            )}
            
            {cart.length > 0 ? 
            <p>
            {cart.every(product => product.type !== "fruit") && <h2>Adicione uma fruta para ser mais saudável</h2>}
            {cart.every(product => product.type === "fruit" || product.type === 'meat') && <h2>Parabéns, sua compra está saudável</h2>}
            {cart.every(product => product.type === "carb") && <h2>Cuidado, muito açucar na dieta é prejudicial a saude</h2>}
            {cart.length}</p> 
            : 
            <h3>Não há items no carrinho</h3>}
        </div>
    )
}
export default Cart;