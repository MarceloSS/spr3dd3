import { useDispatch } from 'react-redux';
import { addToCart, removeFromCart } from '../../store/modules/cart/actions';

const Product = ({product, isRemovable = false}) => {

    const dispatch = useDispatch();
    return (<div>
        <div>{product.name} | <span>{product.type} </span></div>

        {isRemovable ? 
        <button onClick={() => dispatch(removeFromCart(product.id))}>Remover do Carrinho</button>
        :
        <button onClick={() => dispatch(addToCart(product))}>Adicionar ao Carrinho</button>
        }
    </div>)
}
export default Product;