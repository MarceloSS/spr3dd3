import './App.css';

import Products from './components/products'
import Cart from './components/cart'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Products></Products>
        <Cart></Cart>
      </header>
    </div>
  );
}

export default App;
